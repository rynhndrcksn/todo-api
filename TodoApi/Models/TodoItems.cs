﻿using System;
namespace TodoApi.Models
{
    public class TodoItems
    {
        // Properties
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public string Secret { get; set; }


        public TodoItems()
        {

        }
    }
}
